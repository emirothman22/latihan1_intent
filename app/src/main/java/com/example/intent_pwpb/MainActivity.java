package com.example.intent_pwpb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button explictidintent;
    Button implictidintent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        explictidintent = (Button)findViewById(R.id.explicitintend);
        explictidintent.setOnClickListener(this);
        implictidintent = (Button)findViewById(R.id.implictidintend);
        implictidintent.setOnClickListener(this);

    }

    @Override
    public void onClick (View v ){
        switch (v.getId()){
            case R.id.explicitintend:
                Intent explicit = new Intent(MainActivity.this, IntentActivity.class);
                startActivity(explicit);
                break;
            case R.id.implictidintend:
                Intent implicit = new Intent (Intent.ACTION_VIEW, Uri.parse("http://www.w3school.com"));
                startActivity(implicit);
                break;
                default:
                    break;
        }
    }
}
